import { Component, TemplateRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as $ from 'jquery';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UserService } from './user.service';
import { v4 as uuidv4 } from 'uuid';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers:[UserService]
})

export class AppComponent {


  public title = 'frontGatewayB';
  public logado = '' 
  public tokenSalvo = ''
  public formDevice: FormGroup = new FormGroup({
    'deviceId': new FormControl(null, [Validators.required, Validators.email]),
    'key': new FormControl(null, [Validators.required])
  })
  
  

  constructor(
    private userService:UserService,
    private modalService:BsModalService,
    private modalRef:BsModalRef) {}


  public openModalCadastrar(template: TemplateRef<any>){
    this.modalRef = this.modalService.show(template, Object.assign({},{class:'gray modal-lg'}))
    this.formDevice.setValue({
      deviceId:[uuidv4()],
      key:[]
    })
  }

  ngAfterContentInit(){
    $("#menu-toggle").click(function(e) 
    {
      console.log("CLicou")
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });

  }

  ngAfterViewInit(){
    $("#menu-toggle").click(function(e) 
    {
      console.log("CLicou")
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });

  }

  ngOnInit() : void {
                //Toggle Click Function
    $("#menu-toggle").click(function(e) 
    {
      console.log("CLicou")
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });

    this.logado = localStorage.getItem('logado')
    this.tokenSalvo =  localStorage.getItem('token')

    this.userService.checkToken(this.tokenSalvo)
      //.subscribe((valid:boolean)=>{
      .subscribe((valid:boolean)=>{
        
        if(valid){
          localStorage.setItem("logou",'Logado');
          this.logado = localStorage.getItem("logou")
          console.log('esta logado')
          
        }else{
          localStorage.setItem("logou",'naoLogado');
          this.logado = localStorage.getItem("logou")
          console.log('não esta logado')
        }

      },(erro:any)=>{
        console.log(erro)
        alert("ERRO DE CONEXÃO!!\nVerifique sua conexão com a internet!")
        location.reload()
      })
    }
  
public sair(){
  localStorage.setItem("logou",'naoLogado');
  localStorage.setItem("token",'');

  location.reload()
}

public submitFormDevice(){

}

// public toogle(){
//   $("#menu-toggle").click(function(e) 
//   {
//     console.log("CLicou")
//     e.preventDefault();
//     $("#wrapper").toggleClass("toggled");
//   });
// }


}
