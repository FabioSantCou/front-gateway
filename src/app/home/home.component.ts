import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { EstatisticasService } from '../estatisticas.service';
import { Estatisticas } from '../Model/Estatisticas';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers:[EstatisticasService]
})
export class HomeComponent implements OnInit {


  public estatistica1s:Estatisticas
  public estatistica05s:Estatisticas
  public estatistica02s:Estatisticas
  public estatistica01s:Estatisticas
  public estatistica005s:Estatisticas
  public estatistica002s:Estatisticas
  public estatistica001s:Estatisticas
  public estatisticaAut:Estatisticas
  public carregado=false

  constructor(public estatiscaService:EstatisticasService, private spinner:NgxSpinnerService) {
   this.spinner.show() 
  }

  

  ngOnInit(): void {

      this.estatiscaService.getEstatistica1s()
      .subscribe((resp:any)=>{
      console.log(resp)
      this.estatistica1s = resp
      this.estatistica1s = this.normalizaNumeros(this.estatistica1s)
      console.log(this.estatistica1s)
    },(err)=>{
      console.log(err)
    })

    this.estatiscaService.getEstatistica05s()
    .subscribe((resp:any)=>{
      //console.log(resp)
      this.estatistica05s = resp
      this.estatistica05s = this.normalizaNumeros(this.estatistica05s)
    },(err)=>{
      console.log(err)
    })

    this.estatiscaService.getEstatistica02s()
    .subscribe((resp:any)=>{
      //console.log(resp)
      this.estatistica02s = resp
      this.estatistica02s = this.normalizaNumeros(this.estatistica02s)
    },(err)=>{
      console.log(err)
    })


    this.estatiscaService.getEstatistica01s()
    .subscribe((resp:any)=>{
      //console.log(resp)
      this.estatistica01s = resp
      this.estatistica01s = this.normalizaNumeros(this.estatistica01s)
    },(err)=>{
      console.log(err)
    })

    this.estatiscaService.getEstatistica005s()
    .subscribe((resp:any)=>{
      //console.log(resp)
      this.estatistica005s = resp
      this.estatistica005s = this.normalizaNumeros(this.estatistica005s)
    },(err)=>{
      console.log(err)
    })


    this.estatiscaService.getEstatistica002s()
    .subscribe((resp:any)=>{
      //console.log(resp)
      this.estatistica002s = resp
      this.estatistica002s = this.normalizaNumeros(this.estatistica002s)
    },(err)=>{
      console.log(err)
    })


    this.estatiscaService.getEstatistica001s()
    .subscribe((resp:any)=>{
      //console.log(resp)
      this.estatistica001s = resp
      this.estatistica001s = this.normalizaNumeros(this.estatistica001s)
      this.carregado=true
      this.spinner.hide()
    },(err)=>{
      console.log(err)
      this.spinner.hide()

    })

    this.estatiscaService.getEstatisticaAut()
    .subscribe((resp:any)=>{
      console.log(resp)
      this.estatisticaAut = resp
      this.estatisticaAut = this.normalizaNumeros(this.estatisticaAut)
    },(err)=>{
      console.log(err)

    })


  }

  public normalizaNumeros(estatistica:Estatisticas):Estatisticas{
     
    estatistica.media =parseFloat(estatistica.media.toFixed(2))
    estatistica.max =parseFloat(estatistica.max.toFixed(2))
    estatistica.min =parseFloat(estatistica.min.toFixed(2))
    estatistica.desvio =parseFloat(estatistica.desvio.toFixed(2))
    return estatistica
  }

}
