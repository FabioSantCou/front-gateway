import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { User } from '../Model/User';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers:[UserService]
})
export class LoginComponent implements OnInit {

  public user:User
  public token
  public form: FormGroup = new FormGroup({
    'emailForm': new FormControl(null, [Validators.required, Validators.email]),
    'passwordForm': new FormControl(null, [Validators.required])
  })

  constructor(private userService:UserService, private spinnerService : NgxSpinnerService) {
  }

  ngOnInit(): void {

  }

  public submitForm(){

    let user = new User(
      this.form.value.emailForm,
      this.form.value.passwordForm,
    )
    //console.log(this.form.value.emailForm)

    this.userService.loginUser(user)
    .subscribe((resp:any)=>{
      this.token = resp.token
      if(this.token == '')
      {
        localStorage.setItem("logado","naoLogado")
        location.reload()
      }else{
        localStorage.setItem("logado","Logado")
        localStorage.setItem("token",this.token)
        location.reload()
      }
      //console.log(this.token)
      this.spinnerService.hide()
    },(err:any)=>{
      alert('Usuário/Senha inseridos não pertence a uma conta. Verifique e tente novamente.')
      this.spinnerService.hide()
    })
    this.spinnerService.show()
  
  }

}
