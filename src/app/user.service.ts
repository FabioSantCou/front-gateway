import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from './Model/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient){}

    public loginUser(user:User) : Observable<User>
    {

        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
            })
        }

        console.log(user)


        return this.http.post(
            "https://coutmaster.ml/user/login",
            JSON.stringify(user),
            httpOptions
        )
        .pipe(map((resp:any)=>resp))
    }

    public checkToken(tokenRecebido:string) : Observable<boolean>
    {

        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json'
            })
        }
        return this.http.post(
            "https://coutmaster.ml/user/validateToken",
            JSON.stringify({token : tokenRecebido}),
            httpOptions
            )
            .pipe(map((resp:any)=>resp.valid))
        // .toPromise()
        // .then((resp:Response)=>resp.json().valid)
    }
}
