
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Estatisticas } from './Model/Estatisticas';


@Injectable({
  providedIn: 'root'
})
export class EstatisticasService {

  public token = localStorage.getItem('token') 

  constructor(private http: HttpClient){}

    public getEstatistica1s() : Observable<Estatisticas>
    {
       // let tokenSalvo : string = localStorage.getItem('token') 

        // headers.append('Content-type', 'application/json')
        // headers.append('Authorization',tokenSalvo)

        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': this.token
            })
        }

        return this.http.get(
            "https://coutmaster.ml/dado/estatisticas/fog-1s",
            httpOptions
        )
        .pipe(map((resp:any)=>resp))
    }

    public getEstatistica05s() : Observable<Estatisticas>
    {
       // let tokenSalvo : string = localStorage.getItem('token') 

        // headers.append('Content-type', 'application/json')
        // headers.append('Authorization',tokenSalvo)

        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': this.token
            })
        }

        return this.http.get(
            "https://coutmaster.ml/dado/estatisticas/fog-05s",
            httpOptions
        )
        .pipe(map((resp:any)=>resp))
    }

    public getEstatistica02s() : Observable<Estatisticas>
    {
       // let tokenSalvo : string = localStorage.getItem('token') 

        // headers.append('Content-type', 'application/json')
        // headers.append('Authorization',tokenSalvo)

        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': this.token
            })
        }

        return this.http.get(
            "https://coutmaster.ml/dado/estatisticas/fog-02s",
            httpOptions
        )
        .pipe(map((resp:any)=>resp))
    }

    public getEstatistica01s() : Observable<Estatisticas>
    {
       // let tokenSalvo : string = localStorage.getItem('token') 

        // headers.append('Content-type', 'application/json')
        // headers.append('Authorization',tokenSalvo)

        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': this.token
            })
        }

        return this.http.get(
            "https://coutmaster.ml/dado/estatisticas/fog-01s",
            httpOptions
        )
        .pipe(map((resp:any)=>resp))
    }

    public getEstatistica005s() : Observable<Estatisticas>
    {
       // let tokenSalvo : string = localStorage.getItem('token') 

        // headers.append('Content-type', 'application/json')
        // headers.append('Authorization',tokenSalvo)

        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': this.token
            })
        }

        return this.http.get(
            "https://coutmaster.ml/dado/estatisticas/fog-005s",
            httpOptions
        )
        .pipe(map((resp:any)=>resp))
    }

    public getEstatistica002s() : Observable<Estatisticas>
    {
       // let tokenSalvo : string = localStorage.getItem('token') 

        // headers.append('Content-type', 'application/json')
        // headers.append('Authorization',tokenSalvo)

        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': this.token
            })
        }

        return this.http.get(
            "https://coutmaster.ml/dado/estatisticas/fog-002s",
            httpOptions
        )
        .pipe(map((resp:any)=>resp))
    }

    public getEstatistica001s() : Observable<Estatisticas>
    {
       // let tokenSalvo : string = localStorage.getItem('token') 

        // headers.append('Content-type', 'application/json')
        // headers.append('Authorization',tokenSalvo)

        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': this.token
            })
        }

        return this.http.get(
            "https://coutmaster.ml/dado/estatisticas/fog-001s",
            httpOptions
        )
        .pipe(map((resp:any)=>resp))
    }

    public getEstatisticaAut() : Observable<Estatisticas>
    {
       // let tokenSalvo : string = localStorage.getItem('token') 

        // headers.append('Content-type', 'application/json')
        // headers.append('Authorization',tokenSalvo)
        //console.log(this.token)

        const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': this.token
            })
        }

        return this.http.get(
            "https://coutmaster.ml/dado/estatisticas/fog-autenticate",
            httpOptions
        )
        .pipe(map((resp:any)=>resp))
    }
}
