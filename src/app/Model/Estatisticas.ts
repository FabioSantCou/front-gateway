export class Estatisticas {
    constructor(
        public media:Number,
        public min:Number,
        public max:Number,
        public desvio:Number,
        public total:Number
    ){

    }
}